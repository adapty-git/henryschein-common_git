﻿using HenrySchein.Common.Data;
using System;
using System.Collections.Generic;

namespace HenrySchein.Common
{
    public static class UserHelper
    {
        public static string GetMainUserId(string email)
        {
            string mainUserId = string.Empty;

            if (string.IsNullOrWhiteSpace(email))
                return mainUserId;

            try
            {
                var employeeId = ActiveDirectoryHelper.GetEmployeeId(email);
                if (!string.IsNullOrEmpty(employeeId))
                {
                    mainUserId = new AS400DAO().GetMainUserId(employeeId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return mainUserId;
        }
    }
}
