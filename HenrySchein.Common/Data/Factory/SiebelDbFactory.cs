﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HenrySchein.Common.Data.Factory
{
    public class SiebelDbFactory
    {

        #region Constants
        private const string SIBELDB_CONNECTION_STRING_NAME = "SiebelDbConnection";
        #endregion

        #region Attributes

        public static ConnectionStringSettings ConnSettings
        {
            get
            {

                var conn = ConfigurationManager.ConnectionStrings[SIBELDB_CONNECTION_STRING_NAME];

                if (conn == null)
                    throw new Exception("[HenrySchein.Common] Missing app settings key: SiebelDbConnection");

                return conn;
            }
        }

        public static DbProviderFactory Factory
        {
            get { return DbProviderFactories.GetFactory(ConnSettings.ProviderName); }
        }

        #endregion

        public static DbConnection CreateConnection()
        {
            DbConnection cnn = SiebelDbFactory.Factory.CreateConnection();
            cnn.ConnectionString = ConnSettings.ConnectionString;

            return cnn;
        }

        public static DbCommand CreateTextCommand(DbConnection connection, string query)
        {
            DbCommand dbCommand = Factory.CreateCommand();
            dbCommand.CommandText = query;
            dbCommand.CommandType = System.Data.CommandType.Text;
            dbCommand.Connection = connection;
            dbCommand.CommandTimeout = 2000000000;

            return dbCommand;
        }

        public static DbCommand CreateProcedureCommand(DbConnection connection, string procedure)
        {
            DbCommand dbCommand = Factory.CreateCommand();
            dbCommand.CommandText = procedure;
            dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
            dbCommand.Connection = connection;
            dbCommand.CommandTimeout = 2000000000;

            return dbCommand;
        }

        public static DbParameter AddInParameter(string name, object value, DbType type)
        {
            DbParameter param = Factory.CreateParameter();
            param.ParameterName = name;
            param.Value = value;
            param.Direction = System.Data.ParameterDirection.Input;
            param.DbType = type;

            return param;
        }

        public static DbParameter AddOutParameter(string name, int size, DbType type)
        {
            DbParameter param = Factory.CreateParameter();
            param.ParameterName = name;
            param.Direction = System.Data.ParameterDirection.Output;
            param.Size = size;
            param.DbType = type;

            return param;
        }

        public static DbParameter AddOutParameter(string name, DbType type)
        {
            DbParameter param = Factory.CreateParameter();
            param.ParameterName = name;
            param.Direction = System.Data.ParameterDirection.Output;
            param.DbType = type;

            return param;
        }
    }
}
