﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HenrySchein.Common.Data.Factory
{
    public static class AS400DbFactory
    {
        private const string CONNECTION_STRING_NAME = "AS400Db_A_System";

        public static ConnectionStringSettings ConnSettings
        {
            get
            {
                var conn = ConfigurationManager.ConnectionStrings[CONNECTION_STRING_NAME];

                if (conn == null)
                    throw new Exception("[HenrySchein.Common] Missing app settings key: AS400Db_A_System");

                return conn;
            }
        }

        public static DbProviderFactory Factory
        {
            get { return DbProviderFactories.GetFactory(ConnSettings.ProviderName); }
        }

        public static DbConnection CreateConnection()
        {
            DbConnection cnn = AS400DbFactory.Factory.CreateConnection();
            cnn.ConnectionString = ConnSettings.ConnectionString;

            return cnn;
        }

        public static DbCommand CreateTextCommand(DbConnection connection, string query)
        {
            DbCommand dbCommand = Factory.CreateCommand();
            dbCommand.CommandText = query;
            dbCommand.CommandType = System.Data.CommandType.Text;
            dbCommand.Connection = connection;
            dbCommand.CommandTimeout = 2000000000;

            return dbCommand;
        }

        private static DbParameter AddInParameter(string name, object value)
        {
            DbParameter param = Factory.CreateParameter();
            param.ParameterName = name;
            param.Value = value;
            param.Direction = System.Data.ParameterDirection.Input;

            return param;
        }
    }
}
