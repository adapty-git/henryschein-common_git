﻿using HenrySchein.Common.Data.Factory;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HenrySchein.Common.Data
{
    public class AS400DAO
    {
        /// <summary>
        /// Get 
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public string GetMainUserId(string employeeId)
        {
            string mainUserId = string.Empty;

            try
            {
                using (DbConnection connection = AS400DbFactory.CreateConnection())
                {
                    DbCommand command = AS400DbFactory.CreateTextCommand(connection, GetMainUserIdQuery(employeeId));
                    connection.Open();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            mainUserId = reader.IsDBNull(reader.GetOrdinal("userID")) ? string.Empty : reader.GetString(reader.GetOrdinal("userID"));
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return mainUserId;
        }

        public KeyValuePair<string, string> GetSalesOrgAndGroupType(string mainUserId)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();

            try
            {
                using (DbConnection connection = AS400DbFactory.CreateConnection())
                {
                    DbCommand command = AS400DbFactory.CreateTextCommand(connection, GetSalesOrgQuery(mainUserId));
                    connection.Open();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var groupType = reader.IsDBNull(reader.GetOrdinal("groupType")) ? string.Empty : reader.GetString(reader.GetOrdinal("groupType"));
                            var salesOrg = reader.IsDBNull(reader.GetOrdinal("salesOrg")) ? string.Empty : reader.GetString(reader.GetOrdinal("salesOrg"));

                            result = new KeyValuePair<string, string>(groupType, salesOrg);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #region Private Methods

        private string GetMainUserIdQuery(string employeeId)
        {
            return $@"SELECT smtsra as userID                                  
                        FROM hsipcordta.emfadp join hsipcordta.SMFXRF
                        on dpempl = smxrn#                              
                        WHERE trim(DPEMPI) = '{employeeId}'";
        }

        private string GetSalesOrgQuery(string mainUserId)
        {
            return $@"select ga$gty as groupType, ga$slo as salesOrg from hsipdta71.f5550 where ga$gty in (    
                        select gh$gty from hsipdta71.f55109                     
                        where gh$gty <> 'SDFX' and ghtkby = {mainUserId})           
                        and ga$slo in ('F','T','B','C','S')";
        }

        #endregion
    }
}
