﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HenrySchein.Common
{
    public static class ActiveDirectoryHelper
    {
        public static string GetEmployeeId(string email)
        {
            var employeeId = string.Empty;
            var samAccount = string.Empty;
            var description = string.Empty;

            try
            {
                var ldapPath = ConfigurationManager.AppSettings["LDAP_Path"];
                if (ldapPath == null)
                    throw new Exception("[HenrySchein.Common] Missing app settings key: LDAP_Path");

                string filter = string.Format("(&(!userAccountControl:1.2.840.113556.1.4.803:=2)(|(mail={0})(proxyAddresses=SMTP:{1}@henryschein.com)))", email, email.ToLowerInvariant().Replace("@henryschein.com", string.Empty));
                using (var domainPath = new DirectoryEntry(ldapPath))
                {
                    using (var search = new DirectorySearcher(domainPath, filter, new string[] { "SamAccountName", "description" }))
                    {
                        search.ReferralChasing = ReferralChasingOption.All;
                        search.SearchScope = SearchScope.Subtree;
                        var result = search.FindOne();

                        if (result != null)
                        {
                            var resultEntry = new DirectoryEntry();
                            resultEntry = result.GetDirectoryEntry();

                            if (resultEntry.Properties["SamAccountName"].Value != null)
                                samAccount = resultEntry.Properties["SamAccountName"].Value.ToString();

                            if (resultEntry.Properties["description"].Value != null)
                                description = resultEntry.Properties["description"].Value.ToString();

                            //Get the first word of the description otherwise get the SamAccount
                            Regex regEx = new Regex(@"[[:alnum:]]+?((?=\ )|(?=\-)|(?=\/))");
                            employeeId = regEx.Match(description).Value.Trim();

                            //If the description is empty set the SamAccount as the EmployeeId
                            if (string.IsNullOrWhiteSpace(employeeId))
                                employeeId = !string.IsNullOrWhiteSpace(description) ? description : samAccount;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return employeeId;
        }
    }
}
